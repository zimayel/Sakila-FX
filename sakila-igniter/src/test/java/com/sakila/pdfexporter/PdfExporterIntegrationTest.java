/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakila.pdfexporter;

import com.sakila.app.sakila.entities.Customer;
import com.sakila.app.sakila.entities.Payment;
import com.sakila.app.sakila.pdf.report.engine.ReportEngine;
import com.sakila.app.sakila.pdf.report.engine.commons.ReportConfig;
import com.sakila.app.sakila.pdf.report.engine.tools.FileTools;
import com.sakila.app.sakila.pdf.report.itext.ReportTemplateHelper;
import com.sakila.app.sakila.services.CustomerService;
import com.sakila.app.sakila.services.PaymentService;
import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 *
 * @author mayel-1
 */
@RunWith(JUnit4.class)
public class PdfExporterIntegrationTest {

    PaymentService paymentService;
    CustomerService customerService;

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        paymentService = new PaymentService();
        customerService = new CustomerService();
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void TestPDFExportbyCustomer() {
        try {
            ReportEngine reportEngine = new ReportEngine();
            String templatePath = "/templates/payments.vsl";
            String mainDir = FileTools.OS_TEMP_DIR + "TEST-REPOS";
            List<Payment> binding = paymentService.getAllPayments();
            List<Customer> customers = customerService.getAllCustomers();
            Map<String, Object> bindings = new HashMap<>();
            boolean showPDF = false;
            InputStream jarPdf = getClass().getClassLoader().getResourceAsStream("pdfs/psm_report.pdf");
            ReportConfig reportConfig = new ReportConfig();
            reportConfig.setTemplateHelper(new ReportTemplateHelper(jarPdf));
            reportConfig.setStyleLocation("CSS/style_tpl.css");
            reportConfig.setTemplateLocation(templatePath);
            reportConfig.setShowPDF(showPDF);

            for (Customer customer : customers) {
                String customerSubdir = customer.getFirstName() + "_" + customer.getLastName();
                String filePath = mainDir + File.separator + customerSubdir + File.separator + customer.getFirstName() + "_" + customer.getLastName() + ".pdf";
                bindings.put("payments", binding
                        .stream()
                        .filter(pred -> Objects.equals(pred.getCustomerId().getCustomerId(), customer.getCustomerId()))
                        .collect(Collectors.toList()));
                bindings.put("customer", customer);
                reportConfig.setFilePath(filePath);
                reportConfig.setBindings(bindings);
                reportEngine.generatePDF(reportConfig);

            }
        } catch (Exception ex) {
            fail(ex.getMessage());
            System.err.println(ex);
            Logger.getLogger(PdfExporterIntegrationTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
