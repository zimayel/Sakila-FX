/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.airhacks.followme.actors;

import com.sakila.app.sakila.entities.Actor;
import com.sakila.app.sakila.services.ActorService;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javax.inject.Inject;

/**
 *
 * @author mayel-1
 */
public class ActorsPresenter implements Initializable {

    @FXML
    TableView<Actor> actorGrid;
    @FXML
    TableColumn<Actor, Integer> idcolumn;
    @FXML
    TableColumn<Actor, String> firstname;
    @FXML
    TableColumn<Actor, String> lastName;

    @FXML
    TextField firstNameField;
    @FXML
    TextField lastNameField;
    @FXML
    TextField lastUpdatedField;

    @Inject
    ActorService actorService;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        actorGrid.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            firstNameField.setEditable(false);
            lastNameField.setEditable(false);
            lastUpdatedField.setEditable(false);
            firstNameField.setText(actorGrid.getSelectionModel().getSelectedItem().getFirstName());
            lastNameField.setText(actorGrid.getSelectionModel().getSelectedItem().getLastName());
            lastUpdatedField.setText(actorGrid.getSelectionModel().getSelectedItem().getLastUpdate().toLocaleString());
        });
        loadactorList();
    }

    private void loadactorList() {
        try {
            idcolumn.setCellValueFactory(new PropertyValueFactory<>("actorId"));
            firstname.setCellValueFactory(new PropertyValueFactory<>("firstName"));
            lastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
            List<Actor> list = actorService.findAllActors();
            ObservableList<Actor> rows = FXCollections.observableArrayList();
            rows.addAll(list);
            actorGrid.setItems(rows);
        } catch (Exception ex) {
            Logger.getLogger(ActorsPresenter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
