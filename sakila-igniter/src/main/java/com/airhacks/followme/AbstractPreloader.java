/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.airhacks.followme;

import javafx.application.Preloader;
import javafx.scene.Scene;
import javafx.scene.control.ProgressBar;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Zied
 */
public abstract class AbstractPreloader extends Preloader {    

    /** The Progress Bar. */
    protected ProgressBar progressBar;

    /** The preloader Stage. */
    protected Stage preloaderStage;

    /** The text that will display message received. */
    protected Text messageText;

    /** Flag that indicates if the application is initialized. */
    protected boolean appInitialized = false;
    protected  boolean waitforMofication = false;
    @Override
    public void stop() throws Exception {
        super.stop(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public  void start(Stage primaryStage) throws Exception{
        // Store the preloader stage to reuse it later
        this.preloaderStage = primaryStage;

        // Configure the stage
        primaryStage.centerOnScreen();
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        primaryStage.setScene(createPreloaderScene());

        // Let's start the show
        primaryStage.show();
    }
    /**
     * Gets the message from code.
     *
     * @param messageCode the message code
     *
     * @return the message from code
     */
    private String getMessageFromCode(final int messageCode) {
        String res = "";
        switch (messageCode) {
            case 100:
                res = "Initializing";
                break;
            case 200:
                res = "Initialisiere Datenbank";// Provisioned for custom pre-init task
                break;
            case 300:
                res = "Foo";// Provisioned for custom pre-init task
                break;
            case 400:
                res = "Loading Messages Properties";
                break;
            case 500:
                res = "Loading Parameters Properties";
                break;
            case 600:
                res = "Preparing Core Engine";
                break;
            case 700:
                res = "Preloading Resources";
                break;
            case 800:
                res = "Preloading Modules";
                break;
            case 900:
                res = "";// Provisioned for custom post-init task
                break;
            case 1000:
                res = "Starting";
                break;
            default:
        }
        return res;
    }
    /**
     * Creates the preloader scene.
     *
     * @return the scene
     */
    protected abstract Scene createPreloaderScene();
    /**
     * Perform actions before the application start.
     * 
     */
    protected abstract void hideStage();

    @Override
    public void init() throws Exception {
        super.init(); 
    }
    
    @Override
    public boolean handleErrorNotification(ErrorNotification info) {
        return super.handleErrorNotification(info); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void handleApplicationNotification(PreloaderNotification info) {
        if (info instanceof MessageNotification) {
            String infoMessage = ((MessageNotification) info).getMessage();
            int messageCode = 0;
            try {
                messageCode = Integer.valueOf(infoMessage);
            } catch (Exception e) {
                System.out.println("Message ist not a messageCode");                
            }
            if( messageCode > 0){
                infoMessage = getMessageFromCode(messageCode);
            }
                    
            this.messageText.setText((String)infoMessage);
        } else if (info instanceof ProgressNotification) {
            handleProgressNotification((ProgressNotification) info);
        }
    }

    @Override
    public void handleStateChangeNotification(StateChangeNotification event) {
        switch (event.getType()) {
            case BEFORE_LOAD:
                System.out.println(event.getType().name());
                break;
            case BEFORE_INIT:
                System.out.println(event.getType().name());
                this.appInitialized = true;              
                break;
            case BEFORE_START:
                System.out.println(event.getType().name());
                hideStage();
                break;
            default:
        }
    }

    @Override
    public void handleProgressNotification(ProgressNotification info) {
        super.handleProgressNotification(info); 
        this.progressBar.setProgress(info.getProgress());
    }
    
}
