/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.airhacks.followme;

import javafx.animation.ScaleTransition;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 *
 * @author Zied
 */
public class SakilaPreloader extends AbstractPreloader{

    @Override
    protected Scene createPreloaderScene() {
        final StackPane p = new StackPane();

        String imageUrl = getClass().getResource("/backgrounds/19654_en_1.jpg").toExternalForm();
        final ImageView logo = new ImageView(new Image(imageUrl));
        p.getChildren().add(logo);
        StackPane.setAlignment(logo, Pos.CENTER);

        this.progressBar = new ProgressBar(0.0);
        this.progressBar.setPrefSize(460, 20);
        this.progressBar.setCenterShape(true);
        p.getChildren().add(this.progressBar);
        StackPane.setAlignment(this.progressBar, Pos.BOTTOM_CENTER);
        StackPane.setMargin(this.progressBar, new Insets(30));

        this.messageText = new Text("Loading Sakila");
        this.messageText.setFill(Paint.valueOf("FFFFFF"));
        p.getChildren().add(this.messageText);
        StackPane.setAlignment(this.messageText, Pos.BOTTOM_CENTER);
        StackPane.setMargin(this.messageText, new Insets(10));

        return new Scene(p, 600, 200, Color.TRANSPARENT);
    }

    @Override
    protected void hideStage() {
        final Stage stage = this.preloaderStage;

        final ScaleTransition st = new ScaleTransition();
        st.setFromX(1.0);
        st.setToX(0.0);
        st.setDuration(Duration.millis(400));
        st.setNode(stage.getScene().getRoot());
        st.setOnFinished(actionEvent -> stage.hide());
        st.play();
    }
    
}
