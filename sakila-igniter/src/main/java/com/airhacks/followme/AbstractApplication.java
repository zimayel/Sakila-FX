/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.airhacks.followme;

import com.sun.javafx.application.LauncherImpl;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.application.Preloader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 *
 * @author Zied
 * @param <P>
 */
public abstract class AbstractApplication<P extends Pane> extends Application implements ISakilaApplication<P>{

    protected static Preloader preloader;
    
    @Override
    public P rootNode(){
        return null;
    }

    @Override
    public Scene scene(){
        return null;
    }

    @Override
    public  Stage stage(){
        return null;
    }
    
    
    
    /**
     * Launch the Current JavaFX Application with given preloader.
     *
     * @param preloaderClass the preloader class used as splash screen with progress
     * @param args arguments passed to java command line
     */
    protected static void preloadAndLaunch(final Class<? extends Preloader> preloaderClass, final String... args) {
        preloadAndLaunch(getClassFromStaticMethod(3), preloaderClass, args);
    }
    /**
     * Launch the given JavaFX Application with given preloader.
     *
     * @param appClass the JavaFX application class to launch
     * @param preloaderClass the preloader class used as splash screen with progress
     * @param args arguments passed to java command line
     */
    protected static void preloadAndLaunch(final Class<? extends Application> appClass, final Class<? extends Preloader> preloaderClass, final String... args) {
        initPreloader(preloaderClass);
        LauncherImpl.launchApplication(appClass, preloaderClass, args);
        
        
    }
    private static void initPreloader(Class<? extends Preloader> preloaderClass){
        try {
            Constructor<? extends Preloader> c = preloaderClass.getConstructor();
            Preloader p = c.newInstance();
            Field preloaderField = LauncherImpl.class.getDeclaredField("currentPreloader");
            preloaderField.setAccessible(true);
            preloaderField.set(p, p);
            preloader = (Preloader) preloaderField.get(p);
        } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchFieldException ex) {
            Logger.getLogger(AbstractApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * Launch the Given JavaFX Application (without any preloader).
     *
     * @param appClass the JavaFX application class to launch
     * @param args arguments passed to java command line
     */
    protected static void launchNow(final Class<? extends Application> appClass, final String... args) {
        Application.launch(appClass, args);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void init() throws Exception{
        try {
            super.init();
            notifyPreloader(new Preloader.ProgressNotification(0.1));
            preInit();
            postInit();

        } catch (final Exception e) {

        }
    }
    
    private static Class<? extends Application> getClassFromStaticMethod(final int classDeepLevel) {
        Class<? extends Application> clazz = null;
        try {
            clazz = (Class<? extends Application>) Class.forName(Thread.currentThread().getStackTrace()[classDeepLevel].getClassName());
        } catch (final ClassNotFoundException e) {
            clazz = null;
        }
        return clazz;
    }
    /**
     * Perform custom task before application initialization phase.
     */
    protected abstract void preInit();

    /**
     * Perform custom task after application initialization phase and before starting phase.
     */
    protected abstract void postInit();
}
