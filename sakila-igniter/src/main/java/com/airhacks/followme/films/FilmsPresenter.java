/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.airhacks.followme.films;

import com.sakila.app.sakila.entities.Film;
import com.sakila.app.sakila.entities.Language;
import com.sakila.app.sakila.services.FilmService;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import javax.inject.Inject;

/**
 *
 * @author mayel-1
 */
public class FilmsPresenter implements Initializable {

    @FXML TextField name;
    @FXML TextArea desc;
    @FXML TextField releaseYear;
    @FXML TextField language;
    @FXML TextField rentalDuration;
    @FXML TextField rate;
    @FXML TextField ReplacementCost;
    @FXML TextField specialFeatures;
    
    @FXML
    TableView<Film> filmTable;
    @FXML
    TableColumn<Film, Integer> idColumn;
    @FXML
    TableColumn<Film, String> nameColumn;
    @FXML
    TableColumn<Film, String> languageColumn;
    
    @Inject
    FilmService filmService;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setupfilmtableData();
    }

    private void setupfilmtableData() {
        try {
            filmTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
              // TODO fill details view
              name.setText(filmTable.getSelectionModel().getSelectedItem().getTitle());
              desc.setText(filmTable.getSelectionModel().getSelectedItem().getDescription());
              releaseYear.setText(filmTable.getSelectionModel().getSelectedItem().getReleaseYear().toString());
              language.setText(filmTable.getSelectionModel().getSelectedItem().getLanguageName());
              rentalDuration.setText(String.valueOf(filmTable.getSelectionModel().getSelectedItem().getRentalDuration()));
              rate.setText(filmTable.getSelectionModel().getSelectedItem().getRentalRate().toString());
              ReplacementCost.setText(filmTable.getSelectionModel().getSelectedItem().getReplacementCost().toString());
              specialFeatures.setText(filmTable.getSelectionModel().getSelectedItem().getSpecialFeatures());
            });
            
            idColumn.setCellValueFactory(new PropertyValueFactory<>("filmId"));
            nameColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
            languageColumn.setCellValueFactory( (TableColumn.CellDataFeatures<Film, String> film) ->{
                Language l = film.getValue().getLanguageId();                
                return new ReadOnlyStringWrapper( l.getName());
            });            
            List<Film> list = filmService.findAllFilms();
            ObservableList<Film> rows = FXCollections.observableArrayList();
            rows.addAll(list);
            filmTable.setItems(rows);
        } catch (Exception e) {
        }
    }

}
