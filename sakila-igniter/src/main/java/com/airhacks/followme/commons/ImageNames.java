/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.airhacks.followme.commons;

/**
 *
 * @author Zied
 */
public enum ImageNames {
    
    LAUNCH("ic_launch.png"),
    LAUNCH_2X("ic_launch_2x.png"),
    LAUNCH_3X("ic_launch_3x.png");
    
    final String name;
    String iconresource ;

    private ImageNames(String name) {
        this.name = name;
        iconresource = getClass().getResource("/icons").toExternalForm();
    }
    
    public String Path(){
        String imagePath = iconresource +"/"+name;
        System.out.println(imagePath);
        return imagePath;
    }
    
    
}
