package com.airhacks.followme.dashboard;

import com.airhacks.followme.actors.ActorsPresenter;
import com.airhacks.followme.actors.ActorsView;
import com.airhacks.followme.commons.ImageNames;
import com.airhacks.followme.films.FilmsPresenter;
import com.airhacks.followme.films.FilmsView;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;

/**
 *
 * @author adam-bien.com
 */
public class DashboardPresenter implements Initializable {

    @FXML Button menuBtn;
    @FXML BorderPane borderPaneTop;
    @FXML Button allActorsBtn;
    @FXML Button filmListBtn;
    @FXML Button categoriesBtn;
    @FXML StackPane main;

    
    ActorsPresenter actorsPresenter;
    FilmsPresenter filmsPresenter;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        menuBtn.setGraphic(new ImageView(new Image(ImageNames.LAUNCH.Path())));
        allActorsBtn.setOnAction((event) -> {
            onFindAllActorsClicked(event);
        });
        filmListBtn.setOnAction((event)-> {
            onFindAllFilmsClicked(event);
        });
        
        categoriesBtn.setOnAction((event)->{
            onFindAllCategoriesClicked(event);
        });
        
        
    }

    private void onFindAllActorsClicked(ActionEvent event) {
        ActorsView actorsView = new ActorsView();
        actorsPresenter = (ActorsPresenter)actorsView.getPresenter();
        main.getChildren().clear();
        main.getChildren().add(actorsView.getView());
        
        
    }

    private void onFindAllFilmsClicked(ActionEvent event) {
        FilmsView filmsView = new FilmsView();
        filmsPresenter = (FilmsPresenter) filmsView.getPresenter();
        main.getChildren().clear();
        main.getChildren().add(filmsView.getViewWithoutRootContainer());
    }

    private void onFindAllCategoriesClicked(ActionEvent event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    

}
