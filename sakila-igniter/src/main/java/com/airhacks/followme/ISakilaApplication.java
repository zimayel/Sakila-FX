/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.airhacks.followme;

import java.util.List;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 *
 * @author Zied
 */
public interface ISakilaApplication<P extends Pane> {
    /**
     * Return the main stage of this application.
     *
     * @return Returns the stage.
     */
    Stage stage();

    /**
     * Return the scene of the main stage of this application.
     *
     * @return Returns the scene.
     */
    Scene scene();

    /**
     * Return the root node of the main scene of this application.
     *
     * @return Returns the rootNode.
     */
    P rootNode();
    
}
