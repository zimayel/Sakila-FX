/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakila.app.sakila.repositories;

import java.io.Closeable;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceUnit;

/**
 *
 * @author mayel-1
 */
public abstract class AbstractRepository<T> implements Closeable{
    @PersistenceUnit(name = "sakilaPU")
    protected EntityManagerFactory entityManagerFactory;
    
    protected final Logger logger = Logger.getLogger(getClass().getName());

    
    public AbstractRepository() {
        entityManagerFactory = Persistence.createEntityManagerFactory("sakilaPU");
    }
    
    protected EntityManager entityManager(){
        return entityManagerFactory.createEntityManager();
    }
    
    protected T create(T entity){
        try {
            entityManager().persist(entity);
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
        return entity;
    }
    protected void delete(T entity){
        try {
            entityManager().remove(entity);
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
    }

    @Override
    public void close() throws IOException{
        entityManager().close();
    }
    
    
}
