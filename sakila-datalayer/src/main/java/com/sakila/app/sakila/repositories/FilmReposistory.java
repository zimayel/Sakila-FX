/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakila.app.sakila.repositories;

import com.sakila.app.sakila.entities.Film;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 *
 * @author mayel-1
 */
public class FilmReposistory extends AbstractRepository<Film>{

    
    
    public List<Film> findAll(){
        try {
            return entityManager().createNamedQuery("Film.findAll", Film.class).getResultList();
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
        return new ArrayList<>();
    }   
    
}
