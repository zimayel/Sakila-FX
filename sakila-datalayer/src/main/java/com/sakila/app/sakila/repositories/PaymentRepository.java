/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakila.app.sakila.repositories;

import com.sakila.app.sakila.entities.Payment;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 *
 * @author mayel-1
 */
public class PaymentRepository extends AbstractRepository<Payment>{
    
    
    public List<Payment> findAllPayment(){
        int offset = 0 ;
        int max = 1000;
        boolean hasmoreEntries = true;
        List<Payment> offsetList;
        List<Payment> resultList = new ArrayList<>();
        
        try {
            while(hasmoreEntries){
                offsetList = this.entityManager().createNamedQuery("Payment.findAll", Payment.class)
                    .setFirstResult(offset)
                    .setMaxResults(max)
                    .getResultList();
                if(!offsetList.isEmpty()){
                    offset += max;
                    resultList.addAll(offsetList);                    
                }else{
                    hasmoreEntries = false;
                }            
            }
            
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
        return resultList;
    }
}
