/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakila.app.sakila.repositories;

import com.sakila.app.sakila.entities.Actor;
import java.util.List;
import java.util.logging.Level;

/**
 *
 * @author mayel-1
 */
public class ActorRepository extends AbstractRepository<Actor>{

    public List<Actor> findAll(){
        List<Actor> res = null ;
        try {
            res = entityManager().createNamedQuery("Actor.findAll", Actor.class).getResultList();
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
        }        
        return res;
    }
    
    
}
