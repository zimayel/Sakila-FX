/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakila.app.sakila.services;

import com.sakila.app.sakila.entities.Customer;
import com.sakila.app.sakila.repositories.CustomerRepository;
import java.util.List;

/**
 *
 * @author mayel-1
 */
public class CustomerService {
    
    
    public List<Customer> getAllCustomers() throws Exception{
        List<Customer> customers;
        try(CustomerRepository repos = new CustomerRepository()){
            customers = repos.findAllCustomers();
        }catch(Exception exception){
            throw exception;
        }
        return customers;
    }
}
