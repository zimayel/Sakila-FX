/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakila.app.sakila.services;

import com.sakila.app.sakila.entities.Film;
import com.sakila.app.sakila.repositories.FilmReposistory;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mayel-1
 */
public class FilmService {
    
    
    public List<Film> findAllFilms() throws Exception{
        List<Film> films = new ArrayList();
        try(FilmReposistory repos = new FilmReposistory()) {
            films =  repos.findAll();
        } catch (Exception e) {
            throw e;
        }
        return films;
    }
    
}
