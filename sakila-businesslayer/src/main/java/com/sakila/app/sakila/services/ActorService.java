/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakila.app.sakila.services;

import com.sakila.app.sakila.entities.Actor;
import com.sakila.app.sakila.repositories.ActorRepository;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mayel-1
 */
public class ActorService {
    /**
     * return all actors
     * @return 
     * @throws java.lang.Exception 
     */
    public List<Actor> findAllActors() throws Exception{
        List<Actor> resList = new ArrayList<>();
        try (ActorRepository repos = new ActorRepository()){
            resList = repos.findAll();
        } catch (Exception e) {
            throw e;
        }
        return resList;
    }
    
}
