/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakila.app.sakila.services;

import com.sakila.app.sakila.entities.Payment;
import com.sakila.app.sakila.repositories.PaymentRepository;
import java.util.List;

/**
 *
 * @author mayel-1
 */
public class PaymentService {
    
       
    public List<Payment> getAllPayments() throws Exception{
        List<Payment> payments = null;
        try(PaymentRepository paymentRepository = new PaymentRepository()) {
            payments =  paymentRepository.findAllPayment();
        } catch (Exception e) {
            throw e;
        }
        return payments;
    }
    
    
    
}
