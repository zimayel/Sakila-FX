/*
 * Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package com.zfactory.pdf.report.engine;

import com.zfactory.report.engine.ReportEngine;
import com.zfactory.report.engine.commons.ReportConfig;
import com.zfactory.tools.FileTools;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.zfactory.pdf.report.itext.ReportTemplateHelper;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mayel-1
 */
public class ReportEngineTest {

    private String[] colors;

    private String[] brands;

    public ReportEngineTest() {

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        colors = new String[10];
        colors[0] = "Black";
        colors[1] = "White";
        colors[2] = "Green";
        colors[3] = "Red";
        colors[4] = "Blue";
        colors[5] = "Orange";
        colors[6] = "Silver";
        colors[7] = "Yellow";
        colors[8] = "Brown";
        colors[9] = "Maroon";

        brands = new String[10];
        brands[0] = "BMW";
        brands[1] = "Mercedes";
        brands[2] = "Volvo";
        brands[3] = "Audi";
        brands[4] = "Renault";
        brands[5] = "Fiat";
        brands[6] = "Volkswagen";
        brands[7] = "Honda";
        brands[8] = "Jaguar";
        brands[9] = "Ford";

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of GenerateReport method, of class ReportEngine.
     */
    @Test
    public void testGenerateReport() throws Exception {
        System.out.println(">>>>>>>>>>>GenerateReport");
        String templatePath = "/templates/cars.vsl";
        String filePath = FileTools.OS_TEMP_DIR + "TESTENGINE/cars.pdf";
        List<Car> cars = this.createCars(100);
        Map<String, Object> bindings = new HashMap<>();
        bindings.put("cars", cars);
        boolean showPDF = true;
        InputStream jarPdf = getClass().getClassLoader().getResourceAsStream("pdfs/stationery.pdf");
        ReportEngine instance = new ReportEngine();
        ReportConfig reportConfig = new ReportConfig();
        reportConfig.setFilePath(filePath);
        reportConfig.setStyleLocation("CSS/style_tpl.css");
        reportConfig.setTemplateLocation(templatePath);
        reportConfig.setShowPDF(showPDF);
        reportConfig.setBindings(bindings);
        reportConfig.setTemplateHelper(new ReportTemplateHelper(jarPdf));
        instance.generatePDF(reportConfig);
        // TODO review the generated test code and remove the default call to fail.
        assertTrue("File was generated", Files.exists(new File(filePath).toPath()));
        //
        //delete pdf file
        //
        File file = new File(filePath);
        //assertTrue("File deleted", file.delete());
    }

    public List<Car> createCars(int size) {
        List<Car> list = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            list.add(new Car(getRandomId(), getRandomBrand(), getRandomYear(), getRandomColor(), getRandomPrice(), getRandomSoldState()));
        }
        return list;
    }

    private String getRandomId() {
        return UUID.randomUUID().toString().substring(0, 8);
    }

    private int getRandomYear() {
        return (int) (Math.random() * 50 + 1960);
    }

    private String getRandomColor() {
        return colors[(int) (Math.random() * 10)];
    }

    private String getRandomBrand() {
        return brands[(int) (Math.random() * 10)];
    }

    public int getRandomPrice() {
        return (int) (Math.random() * 100000);
    }

    public boolean getRandomSoldState() {
        return (Math.random() > 0.5);
    }

    public List<String> getColors() {
        return Arrays.asList(colors);
    }

    public List<String> getBrands() {
        return Arrays.asList(brands);
    }

}
