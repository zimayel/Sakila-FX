/**
 * Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package com.zfactory.report.engine.commons;

import com.zfactory.pdf.report.itext.TemplateHelper;
import java.util.Map;

/**
 * Created by zmaye on 04.02.2017.
 */
public class ReportConfig {

    private String templateLocation;
    private String styleLocation;
    private String filePath;
    private Map<String, Object> bindings;
    private boolean showPDF;
    private TemplateHelper templateHelper;

    /**
     * Get the template location
     * usually /templates/template.pdf under resources
     *
     * @return  */
    public String getTemplateLocation() {
        return templateLocation;
    }

    /**
     * Set the template location
     * usually /templates/template.pdf under resources
     *
     * @param templateLocation */
    public void setTemplateLocation(String templateLocation) {
        this.templateLocation = templateLocation;
    }

    /**
     * Get the full relative Path of pdf file to be generated
     *
     * @return  */
    public String getFilePath() {
        return filePath;
    }

    /**
     * Set the full relative Path of pdf file to be generated
     *
     * @param filePath */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * Get the Hash Map of the bindings data
     *
     * @return  */
    public Map<String, Object> getBindings() {
        return bindings;
    }

    /****
     * Set the Bindingsdata
     * @param bindings
     */
    public void setBindings(Map<String, Object> bindings) {
        this.bindings = bindings;
    }

    /**
     * A boolean flag for showing the generated PDF
     *
     * @return  **/
    public boolean isShowPDF() {
        return showPDF;
    }

    /**
     * Set a flag for showing the generated PDF
     *
     * @param showPDF */
    public void setShowPDF(boolean showPDF) {
        this.showPDF = showPDF;
    }

    /**
     * get the Template helper
     *
     * @return  */
    public TemplateHelper getTemplateHelper() {
        return templateHelper;
    }

    /**
     * Set a Template Helper
     *
     * @param templateHelper */
    public void setTemplateHelper(TemplateHelper templateHelper) {
        this.templateHelper = templateHelper;
    }

    public String getStyleLocation() {
        return styleLocation;
    }

    public void setStyleLocation(String styleLocation) {
        this.styleLocation = styleLocation;
    }
}
