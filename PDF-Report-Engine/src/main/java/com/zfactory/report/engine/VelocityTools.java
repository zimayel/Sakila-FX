/*
 Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package com.zfactory.report.engine;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;
import java.util.Properties;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.apache.velocity.tools.ToolManager;

/**
 *
 * @author mayel-1
 */
public class VelocityTools {

    private VelocityEngine velocityEngine;
    private VelocityContext velocityContext;
    private Template velocityTemplate;

    /**
     * the first method to be called. Initialize the velocity engine
     *
     * @return
     */
    public VelocityTools initEngine() {
        Properties p = new Properties();
        p.setProperty("resource.loader", "class");
        p.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        velocityEngine = new VelocityEngine();
        velocityEngine.init(p);
        return this;
    }

    /**
     * the second call to be done. Create a template
     *
     * @param templatePath : template path. e.g. templates/test.vsl
     * @return
     */
    public VelocityTools createTemplate(String templatePath) {
        // templates/test.vsl
        velocityTemplate = velocityEngine.getTemplate(templatePath);
        ToolManager velocityToolManager = new ToolManager();
        velocityToolManager.configure("/templates/velocity-tools.xml");
        velocityContext = new VelocityContext(velocityToolManager.createContext());
        return this;
    }

    /**
     * the third call. Fill the context with single data
     *
     * @param key
     * @param value
     * @return
     */
    public VelocityTools fillContext(String key, Object value) {
        if (velocityContext != null) {
            velocityContext.put(key, value);
        }
        return this;
    }

    /**
     * the third call. Fill the context with a Map of data
     *
     * @param bindings
     * @return
     */
    public VelocityTools fillContext(Map<String, Object> bindings) {
        if (velocityContext != null) {
            bindings.entrySet().forEach((entry) -> {
                velocityContext.put(entry.getKey(), entry.getValue());
            });
        }
        return this;
    }

    /**
     * thr fourth and last call. Merge context and return a String as HTML
     *
     * @return a Html rendered result
     * @throws java.io.IOException
     */
    public String mergeContext() throws IOException {
        if (velocityContext != null || velocityContext.getKeys().length > 0) {
            StringWriter stringWriter = new StringWriter();
            if (velocityTemplate != null) {
                velocityTemplate.merge(velocityContext, stringWriter);
                stringWriter.close();
                return stringWriter.toString();
            }
        }
        return "";
    }

}
