/*
 Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package com.zfactory.report.engine;

import com.lowagie.text.DocumentException;
import com.zfactory.report.engine.commons.ReportConfig;
import com.zfactory.pdf.report.itext.TemplateHelper;

import java.awt.Desktop;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author mayel-1
 */
public class ReportEngine {

    /**
     * Generate the template and return an HTML String
     *
     * @param templatePath
     * @param bindings
     * @return a HTML String
     * @throws java.io.IOException
     */
    protected String generateTemplate(String templatePath, Map<String, Object> bindings) throws IOException {

        VelocityTools velocityTools = new VelocityTools();
        return velocityTools
                .initEngine()
                .createTemplate(templatePath)
                .fillContext(bindings)
                .mergeContext();
    }

    /**
     * render HTML to pdf;
     *
     * @param data
     * @param filePath : when not exist will be created with sub directories
     * @throws java.io.IOException
     * @throws java.io.FileNotFoundException
     * @throws com.lowagie.text.DocumentException
     */
    protected void renderTemplate(String data, String filePath) throws IOException, FileNotFoundException, DocumentException, com.itextpdf.text.DocumentException {

        ITextTools iTextTools = new ITextTools();
        iTextTools.renderPDF(data, filePath);
    }
    
    protected  void renderFromHtml(String sourcePath, String destPath, TemplateHelper templateHelper, String style) throws IOException, com.itextpdf.text.DocumentException, DocumentException{
        ITextTools iTextTools = new ITextTools();
        iTextTools.createPdf(sourcePath, destPath, templateHelper, style);
    }

    /**
     * generate a PDF
     * @param reportConfig the initial config
     * @throws com.lowagie.text.DocumentException
     * @throws com.itextpdf.text.DocumentException
     * */
    public void generatePDF(ReportConfig reportConfig) throws DocumentException, com.itextpdf.text.DocumentException, Exception {
        this.validateConfig(reportConfig);
        String data ="";
        try(XMLCleaner cleaner = new XMLCleaner()){
            data = cleaner
                    .stripInvalidXMLCharacters(
                            this.generateTemplate(
                                    reportConfig.getTemplateLocation(),
                                    reportConfig.getBindings()));

            File pdfToExport = new File(reportConfig.getFilePath());
            String fileNameHtml = reportConfig.getFilePath().replace(".pdf", ".html");
            File parent = pdfToExport.getParentFile();
            File htmlFormat = new File(fileNameHtml);

            boolean parent_created = false;
            if (!parent.exists()) {
                parent_created = parent.mkdirs();
            }
            if(parent_created)
                pdfToExport.createNewFile();
            //
            //create the temp Html file
            //
            htmlFormat.createNewFile();
            try (FileOutputStream out = new FileOutputStream(htmlFormat)) {
                out.write(data.getBytes());
            }
            //
            //generate from HTML
            //
            this.renderFromHtml(fileNameHtml, reportConfig.getFilePath(), reportConfig.getTemplateHelper(), reportConfig.getStyleLocation());
            if (reportConfig.isShowPDF()) {
                Desktop.getDesktop().open(pdfToExport);
            }
        } catch (IOException e) {
            Logger.getLogger(this.getClass().getSimpleName()).log(Level.SEVERE, e.getMessage());
        }
    }

    private void validateConfig(ReportConfig reportConfig) throws Exception {
        if(reportConfig == null){
            throw new NullPointerException("The report config can not be NULL");
        }else{
            // if bindfing is empty or null
            if(reportConfig.getBindings() == null || reportConfig.getBindings().isEmpty()){
                throw new Exception("Bindings Map is NULL or Empty");
            }
            if(StringUtils.isEmpty(reportConfig.getFilePath())){
                throw new Exception("File Path is missing");
            }
            if(StringUtils.isEmpty(reportConfig.getStyleLocation())){
                throw new Exception("Stylesheet file location is missing");
            }
            if(StringUtils.isEmpty(reportConfig.getTemplateLocation())){
                throw new Exception("Velocity Template location is missing ");
            }
            if(reportConfig.getTemplateHelper() == null){
                throw new Exception("Template implementation is missing");
            }
        }
        
    }
    
    /**
     * Utility class for stripping invalid characters from XML content. For
     * example the messages might contain some XML inputs that
     * might throw off the SAX parser.
     */
    public class XMLCleaner implements Closeable{
        public String stripInvalidXMLCharacters(String in) {
            StringBuilder builder = new StringBuilder(); // Used to hold the output.
            char current; // Used to reference the current character.

            if (in == null || ("".equals(in))) return ""; // vacancy test.
            for (int i = 0; i < in.length(); i++) {
                current = in.charAt(i);
                if ((current == 0x9) ||
                    (current == 0xA) ||
                    (current == 0xD) ||
                    ((current >= 0x20) && (current <= 0xD7FF)) ||
                    ((current >= 0xE000) && (current <= 0xFFFD)) ||
                    ((current >= 0x10000) && (current <= 0x10FFFF)))
                    builder.append(current);
            }
            return builder.toString();
        }
        @Override
        public void close() throws IOException {
            System.gc();
        }
    }
}
