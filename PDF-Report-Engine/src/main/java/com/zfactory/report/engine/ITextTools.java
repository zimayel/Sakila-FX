/*
 Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package com.zfactory.report.engine;

import com.lowagie.text.DocumentException;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import com.zfactory.pdf.report.itext.ReportTemplateHelper;
import org.xhtmlrenderer.pdf.ITextRenderer;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.zfactory.pdf.report.itext.TemplateHelper;
import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.ElementList;
import com.itextpdf.tool.xml.html.Tags;
import java.io.InputStream;

/**
 *
 * @author mayel-1
 */
public class ITextTools {
    
    public static final String PDF = "pdfs/report_tpl.pdf";
    public static final String CSS = "CSS/style_tpl.css";

    /**
     * The Easy way
     * @param data : the rendered html data returned frm the vilocity engine
     * @param filePath : the path of the created pdf file e.g.
     * C:/reports/test-file.pdf
     * @throws java.io.FileNotFoundException
     * @throws com.lowagie.text.DocumentException
     */
    public void renderPDF(String data, String filePath) throws FileNotFoundException, IOException, DocumentException {
        ITextRenderer renderer = new ITextRenderer();
        renderer.setDocumentFromString(data);
        renderer.layout();
        try (FileOutputStream outputStream = new FileOutputStream(filePath)) {
            renderer.createPDF(outputStream);
        }
    }

    /**
     * The easy Way with layouted page
     * render PDf from HTML and optimize layout
     * @param sourceHtmlPath
     * @param destinationPdfPath
     * @throws java.io.IOException
     * @throws com.itextpdf.text.DocumentException
     */
    public void RenderPdfFromHtml(String sourceHtmlPath, String destinationPdfPath) throws IOException, com.itextpdf.text.DocumentException {

        if (Files.exists(new File(sourceHtmlPath).toPath()) && Files.exists(new File(sourceHtmlPath).toPath())) {

            // step 1
            Document document = new Document();            
            // step 2
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(destinationPdfPath));
            // step 3
            document.open();
            // step 4
            XMLWorkerHelper.getInstance().parseXHtml(writer, document,
                    new FileInputStream(sourceHtmlPath));
            // step 5
            document.close();            
        }
    }
    /**
     * The Entreprise way. generate a PDF based on a PDF template
     * Create Layouted PDF with pagination.
     *
     * you need to have a pre-configuration:
     * 1) "pdfs/report_tpl.pdf" your pdf template under resources
     * 2) /CSS/style_tpl.css" you css style under resources
     *
     * @param sourceHtml the Html source
     * @param destPdf the destination
     * @param template : A template helper collecting all needed informations
     * @param styleLocation
     * @throws java.io.IOException
     * @throws com.lowagie.text.DocumentException
     * @throws com.itextpdf.text.DocumentException
     */
    public void createPdf(String sourceHtml, String destPdf, TemplateHelper template, String styleLocation) throws IOException, DocumentException, com.itextpdf.text.DocumentException {
        //InputStream jarPdf = getClass().getClassLoader().getResourceAsStream(PDF);

        //ReportTemplateHelper template = new ReportTemplateHelper(jarPdf);
        /**
        template.setSender("Max Mustermann\n"
                + "MusterStr\n"
                + "9040 Sint-Amandsberg\n"
                + "BELGIUM");
        template.setReceiver("Z factory\n"
                + "Garten Str 3\n"
                + "67466\n"
                + "Darmstadt Germany");
         */
        // step 1
        Document document = new Document(template.getPageSize(),
            template.getmLeft(), template.getmRight(), template.getmTop(), template.getmBottom());
        // step 2
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(destPdf));
        writer.setPageEvent(template);
        // step 3
        document.open();
        // step 4
        ElementList elements = TemplateHelper.parseHtml(sourceHtml, styleLocation, Tags.getHtmlTagProcessorFactory());
        for (Element e : elements) {
            document.add(e);
        }
        // step 5
        document.close();
    }
}
