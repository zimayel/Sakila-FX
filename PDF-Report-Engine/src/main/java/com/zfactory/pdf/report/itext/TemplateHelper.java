/*
 Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package com.zfactory.pdf.report.itext;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.ElementList;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.css.CssFile;
import com.itextpdf.tool.xml.css.StyleAttrCSSResolver;
import com.itextpdf.tool.xml.html.TagProcessorFactory;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.ElementHandlerPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;
 
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
/**
 *
 * @author mayel-1
 */
public class TemplateHelper extends PdfPageEventHelper {

    protected float mLeft, mRight, mTop, mBottom;
    // initialized in constructor
    protected PdfReader reader;
    protected Rectangle pageSize;
    protected BaseFont basefont;
    protected Font font;
    protected Rectangle footer;
    // initialized upon opening the document
    protected PdfTemplate background;
    protected PdfTemplate total;
    protected PdfContentByte canvas;
    protected ColumnText columnTxt;
    public TemplateHelper(InputStream stationery) throws IOException, DocumentException {
        reader = new PdfReader(stationery);
        pageSize = reader.getPageSize(1);
        basefont = BaseFont.createFont();
        font = new Font(basefont, 12);
    }
 

 
    public Rectangle getPageSize() {
        return pageSize;
    }

    @Override
    public void onOpenDocument(PdfWriter writer, Document document) {
        background = writer.getImportedPage(reader, 1);
        total = writer.getDirectContent().createTemplate(30, 15);
    }
 
    @Override
    public void onEndPage(PdfWriter writer, Document document) {
        canvas = writer.getDirectContentUnder();
        // background
        canvas.addTemplate(background, 0, 0);
        columnTxt = new ColumnText(canvas);
        try {
            // footer (page X of Y)
            columnTxt.setSimpleColumn(footer);
            columnTxt.addText(new Chunk("page " + writer.getPageNumber(), font));
            columnTxt.addText(new Chunk(Image.getInstance(total), 0, 0));
            columnTxt.go();

        } catch (DocumentException e) {
            // can never happen, but if it does, we want to know!
            throw new ExceptionConverter(e);
        }
    }
 
    @Override
    public void onCloseDocument(PdfWriter writer, Document document) {
        // we only know the total number of pages at the moment the document is closed.
        String s = "/" + (writer.getPageNumber() - 1);
        Phrase p = new Phrase(12, s, font);
        ColumnText.showTextAligned(total, Element.ALIGN_LEFT, p, 0.5f, 0, 0);
    }

    /**
     * Pase Html content
     * @param content the Html content
     * @param style css style
     * @param tagProcessors the tag processor
     * @return 
     * @throws java.io.IOException
     * */
    public static ElementList parseHtml(String content, String style, TagProcessorFactory tagProcessors) throws IOException {
        // CSS
        CSSResolver cssResolver = new StyleAttrCSSResolver();
        InputStream cssStream = TemplateHelper.class.getResourceAsStream(style);
        CssFile cssFile = XMLWorkerHelper.getCSS(cssStream);
        cssResolver.addCss(cssFile);
        // HTML
        HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
        htmlContext.setTagFactory(tagProcessors);
        htmlContext.autoBookmark(false);
        // Pipelines
        ElementList elements = new ElementList();
        ElementHandlerPipeline end = new ElementHandlerPipeline(elements, null);
        HtmlPipeline html = new HtmlPipeline(htmlContext, end);
        CssResolverPipeline css = new CssResolverPipeline(cssResolver, html);
        // XML Worker
        XMLWorker worker = new XMLWorker(css, true);
        XMLParser p = new XMLParser(worker);
 
        p.parse(new FileInputStream(content),Charset.forName("cp1252"));
        return elements;
    }
    public float getmLeft() {
        return mLeft;
    }

    public float getmRight() {
        return mRight;
    }

    public float getmTop() {
        return mTop;
    }

    public float getmBottom() {
        return mBottom;
    }
}
