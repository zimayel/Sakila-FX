/**
 * Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package com.zfactory.pdf.report.itext;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by zmayel on 04.02.2017.
 * only for test purposes 
 */
public class ReportTemplateHelper extends TemplateHelper {

    // initialized with setter
    protected String sender = "";
    protected String receiver = "";

    protected Rectangle body;
    protected Rectangle to;
    protected Rectangle from;
    protected Rectangle date;

    protected String today;

    public ReportTemplateHelper(InputStream stationery) throws IOException, DocumentException {
        super(stationery);
        AcroFields fields = reader.getAcroFields();
        body = fields.getFieldPositions("body").get(0).position;

        /** used to create a document*/
        mLeft = body.getLeft() - pageSize.getLeft();
        mRight = pageSize.getRight() - body.getRight();
        mTop = pageSize.getTop() - body.getTop();
        mBottom = body.getBottom() - pageSize.getBottom();

        to = fields.getFieldPositions("to").get(0).position;
        from = fields.getFieldPositions("from").get(0).position;
        date = fields.getFieldPositions("date").get(0).position;
        footer = fields.getFieldPositions("footer").get(0).position;
    }

    @Override
    public void onEndPage(PdfWriter writer, Document document) {
        super.onEndPage(writer, document);

        try {
            // date
            columnTxt.setSimpleColumn(date);
            columnTxt.addText(new Chunk(today, font));
            columnTxt.go();
            // from address
            columnTxt.setSimpleColumn(from);
            columnTxt.addElement(new Paragraph(sender, font));
            columnTxt.go();
            // to address
            columnTxt.setSimpleColumn(to);
            columnTxt.addElement(new Paragraph(receiver, font));
            columnTxt.go();

        } catch (DocumentException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getMessage());
        }

    }

    @Override
    public void onOpenDocument(PdfWriter writer, Document document) {
        super.onOpenDocument(writer, document);
        today = DateFormat.getDateInstance(DateFormat.LONG, Locale.getDefault()).format(Date.from(Instant.now()));
    }

    public void setSender(String sender) {
        this.sender = sender;
    }
    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

}
